output "s3_bucket_name" {
  value = "${aws_s3_bucket.global_trail_bucket.bucket}"
}

output "cloudwatch_log_group_name" {
  value = "${aws_cloudwatch_log_group.global_trail_group.name}"
}

output "cloudwatch_log_group_arn" {
  value = "${aws_cloudwatch_log_group.global_trail_group.arn}"
}