variable "region" {
  description = "A chosen region for the infrastructure"
}

variable "account_id" {
  description = "An account ID"
}

variable "application_name" {
  description = "A custom name for the application"
}

variable "alarms_topic_arn" {
  description = "An ARN of the SNS topic to send alarms to"
}

variable "cloudwatch_retention" {
  description = "A retention in days for the CloudWatch Logs group"
  default = 14
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type = "map"
  default = {}
}