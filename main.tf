data "template_file" "s3_policy_template" {
  template = "${file("${path.module}/policies/s3.tpl.json")}"

  vars {
    bucket_name = "${terraform.workspace}-${lower(var.application_name)}-cloudtrail"
  }
}

resource "aws_s3_bucket" "global_trail_bucket" {
  bucket = "${terraform.workspace}-${lower(var.application_name)}-cloudtrail"
  policy = "${data.template_file.s3_policy_template.rendered}"

  versioning {
    enabled = true
  }

  lifecycle_rule {
    enabled = true

    expiration {
      days = 365
    }

    noncurrent_version_expiration {
      days = 365
    }
  }

  tags = "${var.tags}"
}

resource "aws_iam_role" "cloudwatch_role" {
  assume_role_policy = "${file("${path.module}/policies/cloudwatch_assume_role.json")}"
}

data "template_file" "cloudwatch_role_policy_template" {
  template = "${file("${path.module}/policies/cloudwatch.tpl.json")}"

  vars {
    aws_region = "${var.region}"
    aws_account_id = "${var.account_id}"
    log_group_id = "${aws_cloudwatch_log_group.global_trail_group.id}"
  }
}

resource "aws_iam_role_policy" "cloudwatch_role_policy" {
  role = "${aws_iam_role.cloudwatch_role.id}"
  policy = "${data.template_file.cloudwatch_role_policy_template.rendered}"
}

resource "aws_cloudwatch_log_group" "global_trail_group" {
  name = "${terraform.workspace}-${var.application_name}-cloudtrail"
  retention_in_days = "${var.cloudwatch_retention}"

  tags = "${var.tags}"
}

resource "aws_cloudtrail" "global_trail" {
  depends_on = ["aws_iam_role_policy.cloudwatch_role_policy"]

  name = "${terraform.workspace}-${var.application_name}-account-trail"
  s3_bucket_name = "${aws_s3_bucket.global_trail_bucket.bucket}"
  cloud_watch_logs_group_arn = "${aws_cloudwatch_log_group.global_trail_group.arn}"
  cloud_watch_logs_role_arn = "${aws_iam_role.cloudwatch_role.arn}"

  include_global_service_events = true

  event_selector {
    read_write_type = "All"
    include_management_events = true
  }

  tags = "${var.tags}"
}

module "cloudtrail_api_alarms" {
  source = "git::https://github.com/cloudposse/terraform-aws-cloudtrail-cloudwatch-alarms.git?ref=0.1.1"

  region = "${var.region}"
  log_group_name = "${aws_cloudwatch_log_group.global_trail_group.name}"
  sns_topic_arn = "${var.alarms_topic_arn}"
  metric_namespace = "${var.application_name}"
  create_dashboard = "false"
  add_sns_policy = "false"
}